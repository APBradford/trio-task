# AWS Challenge

This challenge was to create a AWS Network and flask application with the following architecture:

![MVP](https://gitlab.com/APBradford/trio-task/-/raw/master/Architecture2.PNG)

To create this architecute, I created a cloudformation template and then deployed the stack. This template can be found in CloudFormation1.yaml

The CloudFormation designer viewed this architecture as following:

![MVP](https://gitlab.com/APBradford/trio-task/-/raw/master/Architecture.PNG)

The main difference between this architecture and the MVP is the inclusion of a nat gateway in the public subnet to allow the RDS instance in the private subnet internet connectivity.

To run the app, I created and ran two docker containers on the web application EC2. One Container for the flask app and one for the NGINX proxy.

![MVP](https://gitlab.com/APBradford/trio-task/-/raw/master/Containers.PNG)

Here you can see the flask app successfully working via port 80 and having retrieved the data from the rds database:

![MVP](https://gitlab.com/APBradford/trio-task/-/raw/master/RunningApp.PNG)


# Stretch Goals

As a stretch goal, I will attempt to create the following architecture to introduct high availability into my network.
This will introduce an application load balancer to distribute traffic accross two ec2 autoscaling groups across two availability zones. I will also introduce a multi-az deployment for the RDS instance which will create a read replica of the database incase of failiure.

![Stretch](https://gitlab.com/APBradford/trio-task/-/raw/master/High_availability.PNG)

