from app import db
from app import Users

db.drop_all()
db.create_all()

one = Users(first_name="Ben", last_name="Hesketh", email="test@test7.com")
two = Users(first_name="Luke", last_name="Benson", email="test@test.com")
three = Users(first_name="Matt", last_name="Hunt", email="test@test4.com")
db.session.add(one)
db.session.add(two)
db.session.add(three)
db.session.commit()